import React from 'react'
import { useIdle } from 'react-use'
import Screensaver from './component/Screensaver'

export default function App () {
  const isIdle = useIdle(3e3)

  return (
    <div style={{ width: '800px', margin: '1em auto' }}>
      <div>User is idle: {isIdle ? 'Yes 😴' : 'Nope'}</div>
      <br/>
      <Screensaver active={isIdle}/>
    </div>
  )
}
