import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

export default function Screensaver ({ active }) {
  const maxWidth = 640
  const maxHeight = 480
  const pointSize = 20

  const [coordinates, setCoordinates] = useState({
    x: random(0, maxWidth),
    y: random(0, maxHeight)
  })

  useEffect(() => {
    if (!active) {
      return
    }
    const timer = setInterval(() => {
      draw()
      let xNew = coordinates.x + (Math.random() < 0.5 ? 1 : -1) * pointSize * 1.5
      let yNew = coordinates.y + (Math.random() < 0.5 ? 1 : -1) * pointSize * 1.5
      if (xNew < 0) {
        xNew = coordinates.x
      } else if (xNew > maxWidth) {
        xNew = coordinates.x
      }
      if (yNew < 0) {
        yNew = coordinates.y
      } else if (yNew > maxHeight) {
        yNew = coordinates.y
      }
      setCoordinates({
        x: xNew,
        y: yNew
      })
    }, 100)
    return () => clearInterval(timer)
  }, [active, coordinates])

  function draw () {
    const canvas = document.getElementById('canvas')
    const context = canvas.getContext('2d')
    context.clearRect(0, 0, canvas.width, canvas.height)
    context.fillStyle = getRandomColor()
    context.beginPath()
    context.arc(coordinates.x, coordinates.y, pointSize, 0, Math.PI * 2, true)
    context.fill()
  }

  /**
   * @param {number} min
   * @param {number} max
   * @returns {number}
   */
  function random (min = 0, max = 50) {
    const num = Math.random() * (max - min) + min
    return Math.floor(num)
  }

  function getRandomColor () {
    const letters = '0123456789ABCDEF'
    let color = '#'
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)]
    }
    return color
  }

  if (!active) {
    return (
      <div style={{ padding: '3em', fontSize: '2em', textAlign: 'center' }}>FREEZE!</div>
    )
  }

  return (
    <div>
      <canvas id="canvas" width="640" height="480" style={{ border: '1px red solid' }}/>
      <pre>{JSON.stringify(coordinates)}</pre>
    </div>
  )
}

Screensaver.propTypes = {
  active: PropTypes.bool.isRequired
}
